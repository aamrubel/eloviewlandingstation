package com.elotouch.ic.eloviewlandingstation;

import java.util.HashMap;

public abstract class Area {

	int _id;
	String _name;
	HashMap<String, String> _values;

	public Area(int id, String name) {
		_id = id;
		if (name != null) {
			_name = name;
		}
	}

	public int getId() {
		return _id;
	}

	public String getName() {
		return _name;
	}

	public void addValue(String key, String value) {
		if (_values == null) {
			_values = new HashMap<String, String>();
		}
		_values.put(key, value);
	}

	public String getValue(String key) {
		String value = null;
		if (_values != null) {
			value = _values.get(key);
		}
		return value;
	}

	abstract boolean isInArea(float x, float y);

	abstract float getOriginX();

	abstract float getOriginY();
}
