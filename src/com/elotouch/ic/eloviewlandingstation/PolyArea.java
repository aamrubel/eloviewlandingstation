package com.elotouch.ic.eloviewlandingstation;

import java.util.ArrayList;

public class PolyArea extends Area{

	ArrayList<Float> xpoints = new ArrayList<Float>();
	ArrayList<Float> ypoints = new ArrayList<Float>();

	float _x;
	float _y;

	int _points;

	// bounding box
	int top=-1;
	int bottom=-1;
	int left=-1;
	int right=-1;

	public PolyArea(int id, String name, String coords) {
		super(id,name);

		String[] v = coords.split(",");

		int i=0;
		while ((i+1)<v.length) {
			int x = Integer.parseInt(v[i]);
			int y = Integer.parseInt(v[i+1]);
			float xf = x;
			float yf = y;
			xpoints.add(xf);
			ypoints.add(yf);
			top=(top==-1)?y:Math.min(top,y);
			bottom=(bottom==-1)?y:Math.max(bottom,y);
			left=(left==-1)?x:Math.min(left,x);
			right=(right==-1)?x:Math.max(right,x);
			i+=2;
		}
		_points=xpoints.size();

		xpoints.add(xpoints.get(0));
		ypoints.add(ypoints.get(0));

		computeCentroid();
	}

	public double area() {
		double sum = 0.0;
		for (int i = 0; i < _points; i++) {
			sum = sum + (xpoints.get(i) * ypoints.get(i+1)) - (ypoints.get(i) * xpoints.get(i+1));
		}
		sum = 0.5 * sum;
		return Math.abs(sum);
	}

	public void computeCentroid() {
		double cx = 0.0, cy = 0.0;
		for (int i = 0; i < _points; i++) {
			cx = cx + (xpoints.get(i) + xpoints.get(i+1)) * (ypoints.get(i) * xpoints.get(i+1) - xpoints.get(i) * ypoints.get(i+1));
			cy = cy + (ypoints.get(i) + ypoints.get(i+1)) * (ypoints.get(i) * xpoints.get(i+1) - xpoints.get(i) * ypoints.get(i+1));
		}
		cx /= (6 * area());
		cy /= (6 * area());
		_x=Math.abs((int)cx);
		_y=Math.abs((int)cy);
	}


	@Override
	public float getOriginX() {
		return _x;
	}

	@Override
	public float getOriginY() {
		return _y;
	}

	
	public boolean isInAreaOld(float testx, float testy)
	{
		int i, j;
		boolean c = false;
		for (i = 0, j = _points-1; i < _points; j = i++) {
			if ( ((ypoints.get(i)>testy) != (ypoints.get(j)>testy)) &&
				(testx < (xpoints.get(j)-xpoints.get(i)) * (testy-ypoints.get(i)) / (ypoints.get(j)-ypoints.get(i)) + xpoints.get(i)) )
				c = !c;
		}
		return c;
	}
	
	public boolean isInArea(float testx, float testy)
	{
		int i, j;
		boolean c = false;
		for (i = 0, j = _points-1; i < _points; j = i++) {
			if ( ((ypoints.get(i)>=testy) != (ypoints.get(j)>=testy)) &&
				(testx <= (xpoints.get(j)-xpoints.get(i)) * (testy-ypoints.get(i)) / (ypoints.get(j)-ypoints.get(i)) + xpoints.get(i)) )
				c = !c;
		}
		return c;
	}
}
