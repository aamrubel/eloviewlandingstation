package com.elotouch.ic.eloviewlandingstation;


public class RectArea extends Area {

	float _left;
	float _top;
	float _right;
	float _bottom;


	RectArea(int id, String name, float left, float top, float right, float bottom) {
		super(id,name);
		_left = left;
		_top = top;
		_right = right;
		_bottom = bottom;
	}

	public boolean isInArea(float x, float y) {
		boolean ret = false;
		if ((x > _left) && (x < _right)) {
			if ((y > _top) && (y < _bottom)) {
				ret = true;
			}
		}
		return ret;
	}

	public float getOriginX() {
		return _left;
	}

	public float getOriginY() {
		return _top;
	}
}
