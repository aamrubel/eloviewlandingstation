package com.elotouch.ic.eloviewlandingstation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;

public class EloViewLandingActivity extends ActionBarActivity implements
		 OnTouchListener {

	public static final String ELO_INSTALLED_FOLDER = "/sdcard/elo/installed_content";
	public static final String ELO_LANDING_FOLDER = "/sdcard/elo/landed_content";
	public static final String ACTION_INSTALL_PACKAGE_SILENT = "android.intent.action.INSTALL_PACKAGE_SILENT";

	private static final String apkName0 = "Lowe's.apk";//"PayPointVideo.apk";
	private static final String apkName1 = "Pinterest.apk";//"PayPointVideo.apk";
	private static final String apkName2 = "Houzz.apk";//"cube-game-1.7-www.apk4fun.com.apk";
	private static final String apkName3 = "Angry Birds.apk";//MoboMarket.apk";
	private static final String apkName4 = "WebViewController.apk";
	private static final String apkName5 = "moen_experience1.1.apk";//"moen_experience1.1.apk"; //"Candy Crush Saga.apk";//
	
	private static final String apkPackageNameLS = "com.elotouch.ic.eloviewlandingstation";
	private static final String apkPackageName0 = "com.lowes.android";
	private static final String apkPackageName1 = "com.pinterest";
	private static final String apkPackageName2 = "com.houzz.app";
	private static final String apkPackageName3 = "com.rovio.angrybirds";
	private static final String apkPackageName4 = "com.elotouch.ic.webviewcontroller";
	private static final String apkPackageName5 = "com.criticalmass.Moen_Merchandising";
	private static final String apkPackageName6 = "com.king.candycrushsaga";
	private static final String apkPackageName7 = "com.sears.android";
	
	private static final String apkPath = "";


	private RectArea raApp1, raApp2, raApp3, raApp4, raApp5;
	private PolyArea paApp1, paApp2, paApp0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/* create a full screen window */
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_elo_view_landing);

		View fullView;
		fullView = (View)findViewById(R.id.fullView);
		fullView.setOnTouchListener(this);
		
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		size.x = display.getWidth();
		size.y = display.getHeight();
		int deviceInSize = 15; // and 21
		if((size.x >= 1250 && size.x <= 1350) || (size.y >= 750 && size.y <= 850)) // size.x == 1280 || size.y == 800
			deviceInSize = 10;
		else if((size.x >= 1850 && size.x <= 1950) || (size.y >= 1000 && size.y <= 1100)) // size.x == 1920 || size.y == 1080 || size.y == 1032
			deviceInSize = 15;
		if(deviceInSize == 15) {
			paApp0 = new PolyArea(1000, "app0", "640,380,935,280,1240,280,1240,510,640,510");
			paApp1 = new PolyArea(1001, "app1", "0,0,928,0,928,270,615,370,615,509,0,509");
			paApp2 = new PolyArea(1002, "app2", "945,0,1900,0,1900,509,1255,509,1255,370,945,270");
			raApp3 = new RectArea(1003, "app3", 0, 521, 618, 1040); //
			raApp4 = new RectArea(1004, "app4", 630, 521, 1290, 1040); //
			raApp5 = new RectArea(1005, "app5", 1300, 521, 1920, 1040); //
		}else if(deviceInSize == 10) {// 1920/1280 = 1.5     1080/800 = 1.35
			paApp0 = new PolyArea(1000, "app0", "427,281,624,207,827,207,827,377,426,377");
			paApp1 = new PolyArea(1001, "app1", "0,0,618,0,618,200,410,274,410,377,0,377");
			paApp2 = new PolyArea(1002, "app2", "630,0,1266,0,1266,377,836,377,836,274,630,200");
			raApp3 = new RectArea(1003, "app3", 0, 385, 412,770); //
			raApp4 = new RectArea(1004, "app4", 420,385, 860,770); //
			raApp5 = new RectArea(1005, "app5", 866,385, 1280,770); //
		}
		
		List<Area> appsTouchableArea = new ArrayList<Area>();
		appsTouchableArea.add(paApp0);
		appsTouchableArea.add(paApp1);
		appsTouchableArea.add(paApp2);
		appsTouchableArea.add(raApp3);
		appsTouchableArea.add(raApp4);
		appsTouchableArea.add(raApp5);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.elo_view_landing, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static boolean isAppInstalled(String packageName, Context context) {
		PackageManager pManager = context.getPackageManager();
		ApplicationInfo pInfo;
		try {
			pInfo = pManager.getApplicationInfo(packageName, 0);
			return true;
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}
	}

	private void installApk(String apkName) {
		final String uri = ELO_INSTALLED_FOLDER + "/" + apkName;
		File f = new File(uri);
		if (!f.exists()) {
			AssetManager assetManager = EloViewLandingActivity.this.getAssets();
			InputStream in = null;
			OutputStream out = null;
			try {
				in = assetManager.open(apkName);
				out = new FileOutputStream(uri);
				byte[] buffer = new byte[1024];
				int read;
				while ((read = in.read(buffer)) != -1) {
					out.write(buffer, 0, read);
				}
				in.close();
				in = null;
				out.flush();
				out.close();
				out = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (!isApkInstalled(uri, EloViewLandingActivity.this)) {
			silent_install(uri, EloViewLandingActivity.this);
		}
	}

	public static boolean isApkInstalled(String uri, Context context) {
		PackageManager pManager = context.getPackageManager();
		PackageInfo pInfo = pManager.getPackageArchiveInfo(uri, 0);

		if (pInfo != null) {
			try {
				pManager.getPackageInfo(pInfo.packageName,
						PackageManager.GET_ACTIVITIES);
				return true;
			} catch (NameNotFoundException e) {
				return false;
			}
		} else {
			return false;
		}
	}

	public static void silent_install(String uri, Context ctx) {
		Intent intent = new Intent("PackageManagerIntent");
		intent.setAction(ACTION_INSTALL_PACKAGE_SILENT);
		intent.putExtra("PACKAGE_URI", uri);
		ctx.sendBroadcast(intent, "elo.permission.ELO_SECURE");
	}

	public static void launchApp(String uri, Context ctx) {
		PackageManager pManager = ctx.getPackageManager();
		PackageInfo pInfo = pManager.getPackageArchiveInfo(uri, 0);

		if (pInfo == null) {
			if ((ELO_INSTALLED_FOLDER + "/demo.apk").equals(uri)) {
			} else {
				launchApp(ELO_INSTALLED_FOLDER + "/demo.apk", ctx);
			}
		} else {
			Intent launchIntent = pManager
					.getLaunchIntentForPackage(pInfo.packageName);
			if (launchIntent != null) {
				launchIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
						| Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				ctx.startActivity(launchIntent);
			} else {
			}
		}
	}

	public static String getApkFile(String packageName, Context ctx) {
		final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        final List pkgAppsList = ctx.getPackageManager().queryIntentActivities( mainIntent, 0);
        for (Object object : pkgAppsList) {
            ResolveInfo info = (ResolveInfo) object;
            if(info.activityInfo.packageName.equals(packageName)) {
            	File file =new File( info.activityInfo.applicationInfo.publicSourceDir);
            	if(file.exists()) {
            		file.getPath();
            	}
            }
           // copy the .apk file to wherever
        }
		return "";
	}
	
	public static void launchAppProcess(String packageName, Context ctx) {
		PackageManager pManager = ctx.getPackageManager();
		
		Intent launchIntent = pManager.getLaunchIntentForPackage(packageName);
		if (launchIntent != null) {
			launchIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			ctx.startActivity(launchIntent);
			getApkFile(packageName,ctx);
		} 
			
	}
	public void launchApp(int selectedApp) {
		switch (selectedApp) {
		case 0:
			if(isAppInstalled(apkPackageName0, this))
				launchAppProcess(apkPackageName0, this);
			else if (isApkInstalled(ELO_INSTALLED_FOLDER + "/" + apkName0,EloViewLandingActivity.this))
				launchApp(ELO_INSTALLED_FOLDER + "/" + apkName0,EloViewLandingActivity.this);
			else
				installApk(apkName0);
			break;
		case 1:
			if(isAppInstalled(apkPackageName1, this))
				launchAppProcess(apkPackageName1, this);
			if (isApkInstalled(ELO_INSTALLED_FOLDER + "/" + apkName1, EloViewLandingActivity.this))
				launchApp(ELO_INSTALLED_FOLDER + "/" + apkName1, EloViewLandingActivity.this);
			else
				installApk(apkName1);
			break;
		case 2:
			if(isAppInstalled(apkPackageName2, this))
				launchAppProcess(apkPackageName2, this);
			if (isApkInstalled(ELO_INSTALLED_FOLDER + "/" + apkName2,EloViewLandingActivity.this))
				launchApp(ELO_INSTALLED_FOLDER + "/" + apkName2,EloViewLandingActivity.this);
			else
				installApk(apkName2);
			break;
		case 3:
			if(isAppInstalled(apkPackageName3, this))
				launchAppProcess(apkPackageName3, this);
			if (isApkInstalled(ELO_INSTALLED_FOLDER + "/" + apkName3,EloViewLandingActivity.this))
				launchApp(ELO_INSTALLED_FOLDER + "/" + apkName3,EloViewLandingActivity.this);
			else
				installApk(apkName3);
			break;
		case 4:
			if(isAppInstalled(apkPackageName4, this))
				launchAppProcess(apkPackageName4, this);
			if (isApkInstalled(ELO_INSTALLED_FOLDER + "/" + apkName4,EloViewLandingActivity.this))
				launchApp(ELO_INSTALLED_FOLDER + "/" + apkName4,EloViewLandingActivity.this);
			else
				installApk(apkName4);
			break;
		case 5:
			if(isAppInstalled(apkPackageName5, this))
				launchAppProcess(apkPackageName5, this);
			if (isApkInstalled(ELO_INSTALLED_FOLDER + "/" + apkName5,EloViewLandingActivity.this))
				launchApp(ELO_INSTALLED_FOLDER + "/" + apkName5,EloViewLandingActivity.this);
			else
				installApk(apkName5);
			break;
		}
	}
	@Override
	public boolean onTouch(View v, MotionEvent ev) {
		final int action = ev.getAction();
		final int evX = (int) ev.getX();
		final int evY = (int) ev.getY();
		int selectedApp = -1;
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			if(paApp0.isInArea(evX, evY))
				selectedApp = 0;
			if(paApp1.isInArea(evX, evY))
				selectedApp = 1;
			if(paApp2.isInArea(evX, evY))
				selectedApp = 2;
			if(raApp3.isInArea(evX, evY))
				selectedApp = 3;
			if(raApp4.isInArea(evX, evY))
				selectedApp = 4;
			if(raApp5.isInArea(evX, evY))
				selectedApp = 5;
			launchApp(selectedApp);
			break;
		case MotionEvent.ACTION_UP:
			
			break;
		} 
	
		return true;
	}
}
