package com.elotouch.ic.eloviewlandingstation;

public class CircleArea extends Area{

	float _x;
	float _y;
	float _radius;

	CircleArea(int id, String name, float x, float y, float radius) {
		super(id,name);
		_x = x;
		_y = y;
		_radius = radius;

	}

	public boolean isInArea(float x, float y) {
		boolean ret = false;

		float dx = _x-x;
		float dy = _y-y;

		float d = (float)Math.sqrt((dx*dx)+(dy*dy));
		if (d<_radius) {
			ret = true;
		}

		return ret;
	}

	public float getOriginX() {
		return _x;
	}

	public float getOriginY() {
		return _y;
	}
}
